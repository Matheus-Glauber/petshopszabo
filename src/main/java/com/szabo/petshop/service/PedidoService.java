package com.szabo.petshop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.szabo.petshop.model.Pedido;
import com.szabo.petshop.model.enums.TipoPagamentoEnum;
import com.szabo.petshop.repository.PedidoRepository;

/***
 * Responsável por implementar as regras de negócios do Pedido
 * @author Matheus Glauber
 *
 */
@Service
public class PedidoService {

	@Autowired
	private PedidoRepository pedidoRepository;

	@Autowired
	private TipoPagamentoService tipoService;

	
	/***
	 * Método para Inserir Pedido 
	 * É realizado um desconto caso a escolha de pagamento seja a vista ou no cartão
	 * @param pedido
	 * @return
	 */
	public Pedido inserirPedido(Pedido pedido) {
		double desconto;
		double valorPedido;
		tipoService.inserirTipo(pedido.getTipoPagamento());
		pedido.setTipoPagamento(tipoService.getById(pedido.getTipoPagamento().getId()));
		valorPedido = pedido.getValorTotal();
		if (pedido.getTipoPagamento().getNomeTipoPgto().equals(TipoPagamentoEnum.AVISTA)) {
			desconto = 0.15;
				
		}else {
			desconto = 0.10;
		}
		valorPedido -= valorPedido*desconto;
		pedido.setValorTotal(valorPedido);
		return pedidoRepository.save(pedido);
	}
	
	/***
	 * Atualiza um pedido quando é passado um id;
	 * @param pedido
	 * @param id
	 * @return
	 */

	public Pedido update(Pedido pedido, Long id) {
		Pedido ped = getById(id);
		tipoService.inserirTipo(pedido.getTipoPagamento());
		ped.setDataStatus(pedido.getDataStatus());
		ped.setTipoPagamento(tipoService.getById(pedido.getTipoPagamento().getId()));
		return pedidoRepository.save(ped);

	}
	
	/**
	 * Deleta um pedido pelo id
	 * @param id
	 */

	public void delete(Long id) {
		pedidoRepository.deleteById(id);
	}
	
	/***
	 * Metodo que retorna um pedido passando um id
	 * @param id
	 * @return
	 */

	public Pedido getById(Long id) {
		return pedidoRepository.findById(id).get();
	}

	/***
	 * Retorna todos os pedidos ja inseridos
	 * @return
	 */
	public List<Pedido> findAll() {
		List<Pedido> pedidos = pedidoRepository.findAll();
		return pedidos;
	}

}
