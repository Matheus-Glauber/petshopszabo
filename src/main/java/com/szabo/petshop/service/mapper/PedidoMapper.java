package com.szabo.petshop.service.mapper;

import com.szabo.petshop.dto.CadastrarPedidoDTO;
import com.szabo.petshop.model.Pedido;

/***
 * Realiza a conversão do DTO para o objeto
 * @author Matheus Glauber
 *
 */
public class PedidoMapper {
	
	public static Pedido mapper(CadastrarPedidoDTO pedidoDTO) {
		Pedido pedido = new Pedido();
		pedido.setDataStatus(pedidoDTO.getDataStatus());
		pedido.setValorTotal(pedidoDTO.getValorTotal());	
		return pedido;
	}

}
