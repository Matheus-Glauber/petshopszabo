package com.szabo.petshop.service.mapper;

import com.szabo.petshop.dto.CadastrarTrackingDTO;
import com.szabo.petshop.model.Tracking;

/***
 * Realiza a conversão do DTO para o objeto
 * @author Matheus Glauber
 *
 */
public class TrackingMapper {
	
	public static Tracking mapper(CadastrarTrackingDTO trackingDTO) {
		Tracking tracking = new Tracking();
		tracking.setDataStatus(trackingDTO.getDataStatus());
		tracking.setDescricao(trackingDTO.getDescricao());
		tracking.setGeo_long(trackingDTO.getGeo_long());
		tracking.setGeoLat(trackingDTO.getGeoLat());
		return tracking;
	}

}