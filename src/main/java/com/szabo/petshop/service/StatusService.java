package com.szabo.petshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.szabo.petshop.model.Status;
import com.szabo.petshop.repository.StatusRepository;

/***
 * Responsável por implementar o service do Pedido
 * @author Matheus Glauber
 *
 */
@Service
public class StatusService {

	@Autowired
	private StatusRepository statusRepository;
	
	/***
	 * Método para Inserir um Status 
	 * @param status
	 */
	public Status inserirStatus(Status status) {
		return statusRepository.save(status);
	}

	public Status getById(Long id) {
		Status status = statusRepository.findById(id).get();
		return status;
	}

}
