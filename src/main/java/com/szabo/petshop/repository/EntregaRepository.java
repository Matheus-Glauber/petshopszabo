package com.szabo.petshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.szabo.petshop.model.Entrega;

/***
 * Essa Interface é responsavel por criar o repositorio de Entrega, 
 * e através dessa anotação @RepositoryRestResource,
 *  elecria os services e os ednpoints da entrega
 * @author Matheus Glauber
 *
 */
@RepositoryRestResource(collectionResourceRel = "entrega", path="entregas")
public interface EntregaRepository extends JpaRepository<Entrega, Long> {

}