package com.szabo.petshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.szabo.petshop.model.Pedido;

/***
 * Essa Interface é responsavel por criar o repositorio de Pedido, 
 * 
 * @author Matheus Glauber
 *
 */
@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long> {

}
