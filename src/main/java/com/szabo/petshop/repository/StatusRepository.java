package com.szabo.petshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.szabo.petshop.model.Status;

/***
 * Essa Interface é responsavel por criar o repositorio de Status, 
 * 
 * @author Matheus Glauber
 *
 */
@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {

}
