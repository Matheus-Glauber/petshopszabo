package com.szabo.petshop.model.enums;

/***
 * Enum que mostra se o pagamento do cliente é a vista ou no cartão
 * 
 * @author Matheus Glauber
 *
 */
public enum TipoPagamentoEnum {

	CARTAO("Cartao"), AVISTA("A vista");

	private String descricao;

	private TipoPagamentoEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
