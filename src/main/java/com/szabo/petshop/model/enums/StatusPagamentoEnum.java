package com.szabo.petshop.model.enums;

/***
 * Enum responsavel por informar o Status do Pagamento
 * @author Matheus Glauber
 *
 */
public enum StatusPagamentoEnum {
	S("Sim"), N("Nao");

	private String descricao;

	private StatusPagamentoEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
