package com.szabo.petshop.dto;

import java.io.Serializable;
import java.time.LocalDate;

/***
 * DTO para cadastramento de pedidos
 * @author Matheus Glauber
 *
 */
public class CadastrarPedidoDTO implements Serializable {

	private static final long serialVersionUID = 1803435366579067343L;

	private Long idTipoPagamento;

	private Double valorTotal;

	private LocalDate dataStatus;

	public Long getIdTipoPagamento() {
		return idTipoPagamento;
	}

	public void setIdTipoPagamento(Long idTipoPagamento) {
		this.idTipoPagamento = idTipoPagamento;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public LocalDate getDataStatus() {
		return dataStatus;
	}

	public void setDataStatus(LocalDate dataStatus) {
		this.dataStatus = dataStatus;
	}

}
